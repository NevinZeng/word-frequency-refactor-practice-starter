## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

1. Code Review and Show Case.
2. teamwork lecture:This section describes the definition, advantages, and usage of policy patterns.
3. Learning Refactoring.
4. Do the Refactoring exercise.

## R (Reflective): Please use one word to express your feelings about today's class.

- Thoughtful

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

- I think the form of teamwork speech can make everyone participate in it and deepen their impression on the content of the speech. In the process of preparing materials, members of the group can learn the basic content and deepen the concept through sharing and speaking.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

Refactoring through code review during the development process.
