import java.util.*;

import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String CALCULATE_ERROR = "Calculate Error";
    public static final String SPLIT_MARK = "\\s+";

    public String getResult(String inputStr) {
        try {
            List<WordFrequency> wordFrequencies = getWordFrequencies(inputStr, SPLIT_MARK);
            Map<String, List<WordFrequency>> wordCountMap = getWordCountMap(wordFrequencies);
            wordFrequencies = getWordFrequencyWithCount(wordCountMap);
            sortWordFrequencyList(wordFrequencies);
            return formatResult(wordFrequencies);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private static void sortWordFrequencyList(List<WordFrequency> wordFrequencies) {
        wordFrequencies.sort((firstWord, secondWord) -> secondWord.getWordCount() - firstWord.getWordCount());
    }

    private static List<WordFrequency> getWordFrequencyWithCount(Map<String, List<WordFrequency>> wordCountMap) {
        return wordCountMap.entrySet().stream()
                .map(wordCount -> new WordFrequency(wordCount.getKey(), wordCount.getValue().size()))
                .collect(Collectors.toList());
    }

    private static List<WordFrequency> getWordFrequencies(String inputStr, String splitMark) {
        return Arrays.stream(inputStr.split(splitMark))
                .map(value -> new WordFrequency(value, 1))
                .collect(Collectors.toList());
    }

    private static String formatResult(List<WordFrequency> wordFrequencyList) {
        return wordFrequencyList.stream()
                .map(wordFrequency -> String.format("%s %s",wordFrequency.getWord(), wordFrequency.getWordCount()))
                .collect(Collectors.joining("\n"));
    }


    private Map<String, List<WordFrequency>> getWordCountMap(List<WordFrequency> wordFrequencyList) {
        Map<String, List<WordFrequency>> wordCountMap = new HashMap<>();
        wordFrequencyList.forEach(wordFrequency -> wordCountMap.computeIfAbsent(wordFrequency.getWord(), key -> new ArrayList<>())
                                        .add(wordFrequency));
        return wordCountMap;
    }


}
